/*
    Bryan Blue
    12/1/18
    203
    Assignment #5

The Queue class inherits the List class. The queue can have data
enqueued and dequeued from it. Using enqueue, one can add data to
the top of the queue, and using dequeue, returns the bottom of the
queue and removes it. You can also print out the queue
*/
#ifndef QUEUE_H
#define QUEUE_H

#include "List.h"

class Queue : public List
{
    public:
        void enqueue(const string &);
        string dequeue();
        virtual void print() const;
        //friend ostream& operator<<(ostream &, const Queue &);
};

#endif // QUEUE_H
