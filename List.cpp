/*
    Bryan Blue
    12/1/18
    203
    Assignment #5
*/
#include "List.h"

//List constructor
List::List()
{
    head = 0;
    tail = 0;
}

//List overloaded constructor, sets head and tail of
//List to a node with given data
List::List(string data)
{
    head = new Node(data);
    tail = head;
}

//adds a node as a new tail to the list
void List::addLast(string data)
{
    Node *newNode = new Node(data);
    if(isEmpty()) {
        head = newNode;
    } else {
        newNode->setNext(tail);
        tail->setPrev(newNode);
    }
    tail = newNode;
    if(head == 0) {
        head = tail;
    }
}

//adds a node as a new head to the list
void List::addFirst(string data)
{
    Node *newNode = new Node(data);
    if(isEmpty()) {
        tail = newNode;
    } else {
        newNode->setPrev(head);
        head->setNext(newNode);
    }
    head = newNode;
    if(tail == 0) {
        tail = head;
    }
}

//removes tail of the list, sets it to the next one,
//and returns removed data
//throws an error if the list is empty
string List::removeLast()
{
    if(isEmpty()) {
        return "ERROR: List is empty";
    }
    string data = tail->getData();
    Node *temp = tail;
    if(temp->getNext() != 0) {
        tail = tail->getNext();
        tail->setPrev(0);
    } else {
        tail = 0;
        head = 0;
    }
    delete temp;
    return data;
}

//removes head of the list, sets it to the next one,
//and returns removed data
//throws an error if the list is empty
string List::removeFirst()
{
    if(isEmpty()) {
        return "ERROR: List is empty";
    }
    string data = head->getData();
    Node *temp = head;
    if(temp->getPrev() != 0) {
        head = head->getPrev();
        head->setNext(0);
    } else {
        tail = 0;
        head = 0;
    }
    delete temp;
    return data;
}

//returns true if the list is empty, false if not
bool List::isEmpty() const
{
    if(head == 0 && tail == 0) {
        return true;
    } else {
        return false;
    }
}

//returns number of nodes in list
int List::size() const
{
    if(isEmpty()) {
        return 0;
    } else {
        int listSize = 1;
        Node *itr = tail;
        while(itr->getNext() != 0) {
            listSize++;
            itr = itr->getNext();
        }
        return listSize;
    }
}

//prints contents of the list, starting with the tail
void List::print() const
{
    if(isEmpty()) {
        cout << "List is Empty";
    } else {
        Node *itr = tail;
        cout << itr->getData();
        while(itr->getNext() != 0) {
            itr = itr->getNext();
            cout << ", " << itr->getData();
        }
    }
}

//List deconstructor
List::~List()
{
    if(!isEmpty()) {
        Node *curr = tail;
        if(curr->getNext() == 0) {
            delete tail;
            tail = 0;
            head = 0;
        } else {
            while (curr->getNext() !=0) {
                Node *prev = curr;
                curr = curr->getNext();
                while (curr->getNext() != 0)
                {
                    prev = curr;
                    curr = curr->getNext();
                }
                delete curr;
                prev->setNext(0);
                curr = tail;
            }
            delete tail;
            tail = 0;
            head = 0;
        }
    }
}
