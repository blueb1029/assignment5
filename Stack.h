/*
    Bryan Blue
    12/1/18
    203
    Assignment #5

The Stack class inherits the List Class. A stack can have
data pushed or popped to it. Pushing data will put it on
top of the stack, and popping will return the topmost data
and remove it from the list. You can also print out the stack.
*/
#ifndef STACK_H
#define STACK_H

#include "List.h"

class Stack : private List
{
    public:
        void push(string);
        string pop();
        virtual void print() const;
        //friend ostream &operator<<(ostream&, const Stack&);
};

#endif // STACK_H
