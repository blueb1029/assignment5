/*
    Bryan Blue
    12/1/18
    203
    Assignment #5
*/
#include "Queue.h"

//Queue constructor, sets data given to bottom of queue
void Queue::enqueue(const string &name)
{
    addLast(name);
}

//returns first data from queue and removes it
//throws an error if queue is empty
string Queue::dequeue()
{
    if(isEmpty()) {
        throw "ERROR: Queue is empty";
    } else {
        return removeFirst();
    }
}

//prints out contents of stack
void Queue::print() const
{
    List::print();
}

