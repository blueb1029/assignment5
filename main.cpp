/*
    Bryan Blue
    12/1/18
    203
    Assignment #5
*/
#include <iostream>
#include "List.h"
#include "Stack.h"
#include "Queue.h"

using namespace std;

void testNode();
void testNodeError();
void testList();
void testEmptyList();
void testStack();
void testQueue();

int main()
{
    testNode();
    //Uncomment next function to test error handling
    //testNodeError();
    testList();
    testEmptyList();
    testStack();
    testQueue();

    return 0;
}

void testNode() {
    cout << "Testing Node:" << endl;
    Node first = Node("[Original Node Data]");
    cout << first.getData() << ", ";
    first.setData("[Set Node Data]");
    cout << first.getData() << endl;
    Node next = Node("[Next Node Data]");
    first.setNext(&next);
    Node prev = Node("[Prev Node Data]");
    first.setPrev(&prev);
    cout << first.getPrev()->getData() << ", ";
    cout << first.getData() << ", ";
    cout << first.getNext()->getData() << endl;
}

void testNodeError() {
    Node error = Node("!");
}

void testList() {
    cout << endl << "Testing List:" << endl;
    List l = List("Node0");
    l.addFirst("Node1");
    l.addLast("Node-1");
    cout << "List: ";
    l.print();
    cout << endl;
    cout << "List size: " << l.size() << endl;
    cout << "Removed: " << l.removeFirst() << endl;
    cout << "Current list: ";
    l.print();
    cout << endl;
    cout << "Removed: " << l.removeLast() << endl;
    cout << "Current list: ";
    l.print();
    cout << endl;
    cout << "Removed: " << l.removeFirst() << endl;
    cout << "Current list: ";
    l.print();
    cout << endl;
    cout << "List size: " << l.size() << endl;
}

void testEmptyList() {
    cout << endl << "Testing an Empty List:" << endl;
    List emptyL = List();
    if(emptyL.isEmpty()) {
        cout << "Empty list is empty" << endl;
    }
    cout << emptyL.removeFirst() << endl;
    emptyL.print();
    cout << endl;
}

void testStack() {
    Stack s = Stack();
    cout << endl << "Testing Stack:" << endl;
    s.push("First");
    s.push("Second");
    s.push("Third");
    cout << "Stack: ";
    s.print();
    cout << endl;
    cout << "1st Pop: " << s.pop() << endl;
    cout << "2nd Pop: " << s.pop() << endl;
    cout << "3rd Pop: " << s.pop() << endl;
    //Uncomment to test error where you pop an empty stack
    //cout << "4th Pop: " << s.pop() << endl;
}

void testQueue() {
    Queue q = Queue();
    cout << endl << "Testing Queue:" << endl;
    q.enqueue("First");
    q.enqueue("Second");
    q.enqueue("Third");
    cout << "Queue: ";
    q.print();
    cout << endl;
    cout << "1st dequeue: " << q.dequeue() << endl;
    cout << "2nd dequeue: " << q.dequeue() << endl;
    cout << "3rd dequeue: " << q.dequeue() << endl;
    //Uncomment to test error where you pop an empty queue
    //cout << "4th Pop: " << q.dequeue() << endl;
}
