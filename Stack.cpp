/*
    Bryan Blue
    12/1/18
    203
    Assignment #5
*/
#include "Stack.h"

//Stack overloaded constructor, sets name to top of stack
void Stack::push(string name)
{
    addFirst(name);
}

//returns topmost data from stack and removes it
//throws an error if stack is empty
string Stack::pop()
{
    if(isEmpty()) {
        throw "ERROR: Stack is empty";
    } else {
        return removeFirst();
    }
}

//prints out contents of stack
void Stack::print() const
{
    List::print();
}

