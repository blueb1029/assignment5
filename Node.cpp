/*
    Bryan Blue
    12/1/18
    203
    Assignment #5
*/
#include "Node.h"

//Node constructor
Node::Node()
{
    data = "";
    prev = 0;
    next = 0;
}

//Node overloaded constructor, sets data with given string
//the string must not be NULL or less than 3 characters long
Node::Node(string data)
{
    try {
        if (data.size() < 3) {
            throw "ERROR: data cannot be less that 3 characters long";
        } else {
            this->data = data;
            prev = 0;
            next = 0;
        }
    }
    catch(string exceptionString) {
        cout << exceptionString;
    }
}

//returns data
string Node::getData() const
{
    return data;
}

//sets data with given string
//the string must not be NULL or less than 3 characters long
void Node::setData(const string &data)
{
    try {
        if (data.size() < 3) {
            throw "ERROR: data cannot be less that 3 characters long";
        } else {
            this->data = data;
        }
    }
    catch(string exceptionString) {
        cout << exceptionString;
    }
}

//returns the next node
Node * Node::getNext()
{
    return next;
}

//returns the previous node
Node * Node::getPrev()
{
    return prev;
}

//sets next node with given one
void Node::setNext(Node *n)
{
    next = n;
}

//sets previous node with given one
void Node::setPrev(Node *p)
{
    prev = p;
}

//Node deconstructor
Node::~Node()
{
    //dtor
}
