/*
    Bryan Blue
    12/1/18
    203
    Assignment #5

The List class keeps track of the head and tail
of a set of linked nodes. You can add/remove from
the head or tail of the list. You can check if
the list is empty, or what its current size is. and
finally, you can print out the list, which will print
the data of each node in the list, starting with the tail.
*/
#ifndef LIST_H
#define LIST_H

#include "Node.h"

class List
{
    private:
        Node *head;
        Node *tail;
    public:
        List();
        List(string data);

        void addLast(string);
        void addFirst(string);
        string removeLast();
        string removeFirst();

        bool isEmpty() const;
        int size() const;

        virtual void print() const;
        //friend ostream &operator<<(ostream&, const List&);

        virtual ~List();
};

#endif // LIST_H
