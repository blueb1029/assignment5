/*
    Bryan Blue
    12/1/18
    203
    Assignment #5

The Node Class stores data in the form of a string
alongside a reference to a next node and a previous
node, you can get/set the nodes and data.
*/
#ifndef NODE_H
#define NODE_H

#include <string>
#include <iostream>

using namespace std;

class Node {
    private:
        string data;
        Node *prev;
        Node *next;
    public:
        Node();
        Node(string);

        string getData() const;
        void setData(const string &);

        Node *getNext();
        Node *getPrev();
        void setNext(Node *);
        void setPrev(Node *);

        virtual ~Node();
};

#endif // NODE_H
